package stepDefinitions;

import java.io.IOException;
import java.util.Properties;
import base.Base;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utilities.ScreenshotMethod;

public class Hooks extends Base {
	public ScreenshotMethod ss;
	Properties prop;
	String screenshotName = null, screenshotFilePath = null;

	@Before(order = 1)
	public void LaunchChromeBrowser() throws IOException {
		driver = init();
	}

	@Before(order = 2)
	public void LaunchApplication() {
		driver.get("https://www.google.com/");
	}

	@Before(order = 2)
	public void Initialize(Scenario sc) {

	}

	@After(order = 1)
	public void TakeAndAttachScreenshot(Scenario sc) {
		ScreenshotMethod.AttachScreenShotToExtentReport(driver, sc);
	}

	@After(order = 0)
	public void CloseBrowser() throws Throwable {
		Thread.sleep(2000);

		driver.quit();

		System.out.println(" <----------------------- Scenario End ------------------------->");
	}

	/*
	 * @Before public void beforeScenario() throws Throwable { System.out.
	 * println(" <----------------------- Run before the Scenario ------------------------->"
	 * );
	 * 
	 * 
	 * }
	 * 
	 * @After public void afterScenario(Scenario scenario) throws Throwable {
	 * if(scenario.isFailed()) { TakesScreenshot ts = (TakesScreenshot) driver;
	 * byte[] src = ts.getScreenshotAs(OutputType.BYTES); scenario.attach(src,
	 * "image/png", "screenshot"); }
	 * 
	 * Base.driver.quit();
	 * 
	 * System.out.
	 * println(" <----------------------- Run After the Scenario ------------------------->"
	 * ); }
	 */

}
