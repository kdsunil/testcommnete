package stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.*;
import org.apache.logging.log4j.simple.SimpleLogger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import base.Base;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.GooglePage;

public class GoogleTest extends Base {
	
	
	public GooglePage google;
	public Properties prop;
	Logger log = LogManager.getLogger("GoogleTest");

	@Given("Browser is launched")
	public void browser_is_launched() throws IOException {
		prop = new Properties();
		// Write code here that turns the phrase above into concrete actions
		//System.setProperty("webdriver.chrome.driver", ".//Drivers//chromedriver.exe");
//		driver = init();
		google = new GooglePage(driver);
		System.out.println("Below is logger");
//		log.info("Driver launched");
	}

	@When("Open url")
	public void open_url() {
		// Write code here that turns the phrase above into concrete actions
		//driver.get("https://www.google.com/");
		log.info("This is info log");
		log.debug("This is debug log");
		log.warn("This is warn log");
		log.error("This is error log");
		log.fatal("This is fatal log");
		//takeScreenShot("methodname", driver);
		//assertTrue(false);
	}

	@When("Enter keyword")
	public void enter_keyword() {
		// Write code here that turns the phrase above into concrete actions
		
		google.enterKeyword("Success");
	}

	@Then("Verify page opened")
	public void verify_page_opened() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("Page opened");
		//driver.quit();
		System.out.println("Below is logger");
//		log.info("Driver closed");
		//assertTrue(false);
	}

	@When("Enter <{string}> and <{string}>")
	public void enter_and(String suc, String fail) {
		//assertTrue(false);
	    google.enterKeyword(suc);
	    google.enterKeyword(fail);
	}




}
