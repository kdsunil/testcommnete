package Runner;

import org.testng.annotations.DataProvider;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = { ".//Features" }, 
		plugin = { "json:target/cucumber.json", 
			"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" },
		glue = {"stepDefinitions" }, 
		dryRun = false, 
		monochrome = true,
		tags = "@Smoke")

public class TestRunner extends AbstractTestNGCucumberTests  {
	@Override
	@DataProvider(parallel = false)
	public Object[][] scenarios() {
		return super.scenarios();
	}

}

/*
 * For @CucumberOptions, the above would look like: tags = {"@tag"} is unchanged
 * tags = {"~tag"} becomes tags = {"not tag"} tags = {"@tag1,@tag2") becomes
 * tags = {"@tag1 or @tag2"} tags = {"@tag1","@tag2"} becomes tags =
 * {"@tag1 and @tag2"} tags = {"@tag1","@tag2,@tag3"} becomes tags =
 * {"@tag1 and (@tag2 or @tag3)"}
 */
