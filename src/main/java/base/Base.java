package base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Base {

	public Logger log;
	public static WebDriver driver;
	public Properties prop;
	String filePath = System.getProperty("user.dir") + "\\Screenshots\\";

	public WebDriver init() throws IOException {
		log = LogManager.getLogger("Base");
		prop = new Properties();

		FileInputStream configPropFile = new FileInputStream("config.properties");
		prop.load(configPropFile);

		String browsername = prop.getProperty("browser");

		if (browsername.equals("chrome")) {

			
			  System.setProperty("webdriver.chrome.driver",
			  prop.getProperty("chromepath")); 
			  //driver = new ChromeDriver();
			 
			
			  ChromeOptions chromeOptions = new ChromeOptions();
			  
			  chromeOptions.addArguments("--remote-allow-origins=*");
			  
			  driver = new ChromeDriver(chromeOptions);
			  driver.get("https://www.google.com/");
			 
		} else if (browsername.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", prop.getProperty("firefoxdriver"));
			driver = new FirefoxDriver();
		} else if (browsername.equals("ie")) {
			System.setProperty("webdriver.ie.driver", prop.getProperty("iepath"));
			driver = new InternetExplorerDriver();
		}
		log.info("************************** Browser Launched **************************");

		return driver;
	}

//	@Before
//	public void setup() throws IOException {
//		driver = init();
//		driver.get(prop.getProperty("url"));
//	}
//
//	@After
//	public void tearDown() {
//		driver.close();
//		driver.quit();
//	}

	public void takeScreenShot(String methodName, WebDriver driver) {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// The below method will save the screen shot in d drive with test method name
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYYY_HHmmss");
		String sDate = sdf.format(date);
		try {
			FileUtils.copyFile(scrFile, new File(filePath + methodName + "_" + sDate + ".png"));
			System.out.println("***Placed screen shot in " + filePath + " ***");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
