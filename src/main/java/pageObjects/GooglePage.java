package pageObjects;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.WaitHelper;

public class GooglePage {
	
	public WebDriver driver;
	public WaitHelper wait;
	
	public GooglePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		wait = new WaitHelper(driver);
	}

	@FindBy(name="q")
	WebElement searchbar;
	
	@FindBy(name="btnK")
	WebElement searchbutton;
	
	public void enterKeyword(String key) {
		wait.waitExplicitlyForElement(searchbar, Duration.ofSeconds(5));
		searchbar.sendKeys(key);
	}
	
	
	
	public void clickSearch() {
		searchbutton.click();
	}
	
}