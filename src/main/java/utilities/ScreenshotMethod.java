package utilities;


import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.Scenario;

public class ScreenshotMethod {
	String ScreenShotFolderPath = null, ScreenShotPath = null, TimeStamp = null;
	String ScreenShotFolder = null, ScreenShotPassedFolder = null, ScreenShotFailedFolder = null;
	String ConfigKeysFile = "Config_Keys.properties";
	/*
	 * public void ReportScreenshot(WebDriver driver, Scenario scenario) { try {
	 * String screenshotName = scenario.getName().replaceAll(" ", "_"); byte[]
	 * screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
	 * File screenshot_with_scenario_name = ((TakesScreenshot)
	 * driver).getScreenshotAs(OutputType.FILE);
	 * 
	 * System.out.println("Screenshot Name: " + screenshotName);
	 * 
	 * TimeStamp = new
	 * SimpleDateFormat("ddMMMyyyy_HHmmss").format(Calendar.getInstance().getTime())
	 * ; if (scenario.isFailed() == true) { ScreenShotPath = ScreenShotFailedFolder
	 * + "OMS_" + screenshotName + "_" + TimeStamp + ".png"; } else { ScreenShotPath
	 * = ScreenShotPassedFolder + "OMS_" + screenshotName + "_" + TimeStamp +
	 * ".png"; }
	 * 
	 * File destinationPath = new File(ScreenShotPath);
	 * Files.copy(screenshot_with_scenario_name.toPath(), destinationPath.toPath());
	 * 
	 * 
	 * Reporter.addScreenCaptureFromPath(destinationPath.toString());
	 * Reporter.addScenarioLog(screenshotName);
	 * 
	 * scenario.embed(screenshot, "image/png");
	 * 
	 * } catch (Exception Ex) { System.out.println(Ex.getMessage()); }
	 * 
	 * }
	 */

	public static void AttachScreenShotToExtentReport(WebDriver driver, Scenario sc) {
		try {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			sc.attach(screenshot, "image/png", "image");
		} catch (Exception e) {
			System.out.println(e);
			System.out.print("Attach screenshot to extent report Fail");
		}
	}
	
	public String ScreenShot(WebDriver driver, String screenshotFolderPath, String screenshotName) throws Throwable {
		String ScreenShotPath = null;
    	try {
	  	    ScreenShotPath = screenshotFolderPath + screenshotName + "_" + CurrentDate("dd-MM-yyyy_HH.mm.ss") + ".jpg";
	  	    File screenshots = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);       		 
		    FileUtils.copyFile(screenshots, new File(ScreenShotPath));
        }
	  	catch(Exception e) {
	  		System.out.println(e);
	  		System.out.print("Screenshot Fail");
	  	}
	  	return ScreenShotPath;
  	 }
	
	public static String CurrentDate(String Format) {
        SimpleDateFormat dtFormat = new SimpleDateFormat(Format);
        Calendar dtCalender = Calendar.getInstance();
        String dtReturn = dtFormat.format(dtCalender.getTime());
 
        return dtReturn;
    }
}
